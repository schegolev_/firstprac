from methods import barrier_method


def main():
    function = lambda x: 150 * (
            (x[1][0] - 2 * x[0][0]) ** 4 + (x[2][0] - 3 * x[0][0]) ** 4 + (x[3][0] - 4 * x[0][0]) ** 4 +
            (x[4][0] - 5 * x[0][0]) ** 4) + (x[0][0] - 2) ** 4

    restriction_function = lambda x: x[0][0] ** 2 + x[1][0] ** 2 + x[2][0] ** 2 + x[3][0] ** 2 + x[4][0] ** 2 - 363

    r = 100
    b = 0.18

    print("Enter epsilon:")
    epsilon = float(input())
    print("Enter maximum number of iterations:")
    maxiter = int(input())
    print("Enter starting point (function of five variables):")
    x_start = [[0], [0], [0], [0], [0]]
    x_start[0][0] = float(input())
    x_start[1][0] = float(input())
    x_start[2][0] = float(input())
    x_start[3][0] = float(input())
    x_start[4][0] = float(input())
    x_min, iter = barrier_method(function, list(x_start), r, b, epsilon, restriction_function, maxiter)

    print("Minimum point is:")
    print(x_min)

    print("Minimum function value is:")
    print(function(x_min))

    print("Iteration number:")
    print(iter)


if __name__ == '__main__':
    main()
