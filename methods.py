from helpers import *
import matplotlib.pyplot as plt


def dfp(x0, r):
    Q0 = edinmat(5)
    d0 = matrix_min(matrix_mult(Q0, grad(x0, r), 5, 1, 5), 5, 1)
    d0 = matrix_mult_by_num(d0, 0.0001, 5, 1)
    x1 = matrix_sum(x0, d0, 5, 1)
    r0 = matrix_sub(x1, x0, 5, 1)
    s0 = matrix_sub(grad(x1, r), grad(x0, r), 5, 1)
    rkm = r0
    Qkm = Q0
    skm = s0
    xk = x1
    xkm = x0

    for i in range(5000):

        if (modul(grad(xk, r), 5) < 0.01):
            break
        N = matrix_sum(Qkm, matrix_mult_by_num(matrix_mult(rkm, trans(rkm, 5, 1), 5, 5, 1),
                                               (1 / scalar(rkm, skm, 5)), 5, 5), 5, 5)
        Qk = matrix_sub(N, matrix_mult_by_num(
            matrix_mult(matrix_mult(Qkm, skm, 5, 1, 5), trans(matrix_mult(Qkm, skm, 5, 1, 5), 5, 1), 5, 5, 1),
            (1 / scalar(matrix_mult(Qkm, skm, 5, 1, 5), skm, 5)), 5, 5), 5, 5)

        dk = matrix_min(matrix_mult(Qk, grad(xkm, r), 5, 1, 5), 5, 1)
        dk = matrix_mult_by_num(dk, 0.001, 5, 1)

        xkp = matrix_sum(xk, dk, 5, 1)
        rkm = matrix_sub(xkp, xk, 5, 1)
        skm = matrix_sub(grad(xkp, r), grad(xk, r), 5, 1)
        Qkm = Qk
        xkm = xk
        xk = xkp

    return xk


def barrier_method(func, x_c, r, b, eps, restriction_fucntion, maxit):
    i = 1
    bar = lambda x: 1
    spisit = []
    fzn = []
    with open('iterations_table.txt', 'w') as f:
        while i < maxit:

            if bar(x_c) < eps:
                break
            bar = lambda x: r * (1.0 / (restriction_fucntion(x) ** 2))

            x_c = dfp(x_c, r)

            i += 1
            r *= b
            spisit.append(i)
            fzn.append(func(x_c))
            func_val = func(x_c)
            output = 'iteration: {};   arguments value: {};   function value: {}\n\n'.format(i - 1 , x_c, func_val)
            f.write(output)
    plt.plot(spisit,fzn);
    plt.show(); 
    return [x_c, i]
