import math
import random


def scalar(a, b, m):
    s = 0
    for i in range(m):
        s += a[i][0] * b[i][0]
    return s


def modul(s, m):
    return math.sqrt(scalar(s, s, m))


def matrix_sum(mat1, mat2, m, n):
    mat3 = [[random.randrange(10) for _ in range(n)] for _ in range(m)]
    for i in range(m):
        for j in range(n):
            mat3[i][j] = mat1[i][j] + mat2[i][j]
    return mat3


def matrix_sub(mat1, mat2, m, n):
    mat3 = [[random.randrange(10) for _ in range(n)] for _ in range(m)]
    for i in range(m):
        for j in range(n):
            mat3[i][j] = mat1[i][j] - mat2[i][j]
    return mat3


def edinmat(m):
    e = [[random.randrange(10) for _ in range(m)] for _ in range(m)];
    for i in range(m):
        for j in range(m):
            if (i == j):
                e[i][j] = 1
            else:
                e[i][j] = 0
    return e


def trans(matr, m, n):
    newmatr = [[random.randrange(10) for _ in range(m)] for _ in range(n)]
    for i in range(n):
        for j in range(m):
            newmatr[i][j] = matr[j][i]
    return newmatr


def matrix_mult(mat1, mat2, m, n, l):
    mat3 = [[0 for _ in range(n)] for _ in range(m)]
    for i in range(m):
        for j in range(n):
            mat3[i][j] = 0
            for k in range(l):
                mat3[i][j] += mat1[i][k] * mat2[k][j]

    return mat3


def matrix_min(matrix, m, n):
    newmatr = [[0 for _ in range(n)] for _ in range(m)]
    for i in range(m):
        for j in range(n):
            newmatr[i][j] = (matrix[i][j] * (-1))
    return newmatr


def matrix_mult_by_num(matrix, number, m, n):
    newmatr = [[0 for _ in range(n)] for _ in range(m)]
    for i in range(m):
        for j in range(n):
            newmatr[i][j] = (matrix[i][j] * number)
    return newmatr


def grad(x, r):
    grad1 = lambda x: (-600) * (2 * (x[1][0] - 2 * x[0][0]) ** 3 + 3 * (x[2][0] - 3 * x[0][0]) ** 3 + 4 * (
                x[3][0] - 4 * x[0][0]) ** 3 + 5 * (x[4][0] - 5 * x[0][0]) ** 3) + (4 * (x[0][0] - 2)) ** 3 + (
                                   2 * r * x[0][0]) * (-2) / ((x[0][0] ** 2 + 2 * x[1][0] ** 2 + 3 * x[2][0] ** 2 + 4 *
                                                               x[3][0] ** 2 + 5 * x[4][0] ** 2 - 224) ** 3);
    grad2 = lambda x: 600 * (x[1][0] - 2 * x[0][0]) ** 3 + (2 * r * 2 * x[1][0]) * (-2) / (
                (x[0][0] ** 2 + 2 * x[1][0] ** 2 + 3 * x[2][0] ** 2 + 4 * x[3][0] ** 2 + 5 * x[4][0] ** 2 - 224) ** 3);
    grad3 = lambda x: 600 * (x[2][0] - 3 * x[0][0]) ** 3 + (2 * r * 3 * x[2][0]) * (-2) / (
                (x[0][0] ** 2 + 2 * x[1][0] ** 2 + 3 * x[2][0] ** 2 + 4 * x[3][0] ** 2 + 5 * x[4][0] ** 2 - 224) ** 3);
    grad4 = lambda x: 600 * (x[3][0] - 4 * x[0][0]) ** 3 + (2 * r * 4 * x[2][0]) * (-2) / (
                (x[0][0] ** 2 + 2 * x[1][0] ** 2 + 3 * x[2][0] ** 2 + 4 * x[3][0] ** 2 + 5 * x[4][0] ** 2 - 224) ** 3);
    grad5 = lambda x: 600 * (x[4][0] - 5 * x[0][0]) ** 3 + (2 * r * 5 * x[2][0]) * (-2) / (
                (x[0][0] ** 2 + 2 * x[1][0] ** 2 + 3 * x[2][0] ** 2 + 4 * x[3][0] ** 2 + 5 * x[4][0] ** 2 - 224) ** 3);
    return ([[grad1(x)], [grad2(x)], [grad3(x)], [grad4(x)], [grad5(x)]])
